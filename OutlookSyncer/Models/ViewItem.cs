﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookSyncer.Models
{
    public class ViewItem
    {
        public string ViewItemId { get; set; }
        public string ViewItemType { get; set; }
        public string ViewItemClass { get; set; }
        private List<object> values { get; set; }
        public object ViewItemValue { get; set; }
        public object LastValue
        {
            get
            {
                if (values == null) return null;

                return values.LastOrDefault();
            }
        }

        private object locker = new object();


        public void AddValue(object value)
        {
            lock(locker)
            {
                values.Add(value);
            }
        }

        public void ClearValues()
        {
            lock (locker)
            {
                values.Clear();   
            }
        }

        public bool HasChanged
        {
            get
            {
                var result = false;
                lock(locker)
                {
                    result = values != null && values.Any();
                }

                return result;
            }
        }

        public ViewItem()
        {
            values = new List<object>();
        }
    }
}
