﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookSyncer.Models
{
    public class CommandLineArg
    {
        public string RawArgument { get; private set; }
        public string ParamName { get; private set; }
        public string ParamValue { get; private set; }

        public string ErrorMessage { get; set; }
        public bool HasError { get; set; }

        public CommandLineArg(string argument)
        {
            RawArgument = argument;
            ParseArgument();
        }

        private void ParseArgument()
        {
            if (RawArgument.StartsWith("-"))
            {
                if (!RawArgument.Contains("="))
                {
                    HasError = true;
                    ErrorMessage = "Name and Value of commandline argument cannot be recognized!";
                    return;
                }

                var ix = RawArgument.IndexOf("=");

                ParamName = RawArgument.Substring(1, ix - 1);

                ParamValue = RawArgument.Replace("-" + ParamName + "=","");

            }
            else
            {
                ParamValue = RawArgument;
            }
        }
    }
}
