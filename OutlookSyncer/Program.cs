﻿using OutlookSyncer.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookSyncer
{
    class Program
    {
        private static OutlookConnectorController controller;
        private static CommandLineController commandLineController;

        private static bool exit;
        static void Main(string[] args)
        {
            Console.CancelKeyPress += Console_CancelKeyPress;
            commandLineController = new CommandLineController(args);

            if (commandLineController.HasError)
            {
                Console.WriteLine(commandLineController.ErrorMessage);
                Environment.Exit(-1);
            }
            controller = new OutlookConnectorController(commandLineController.Server, 
                commandLineController.Port, 
                commandLineController.UserName, 
                commandLineController.Group,
                commandLineController.Password);
            controller.finishedSync += Controller_finishedSync;
            exit = false;
            while (!exit)
            {
              

            }

            Console.WriteLine("Press key for exit");
            Console.ReadKey();
        }

        private static void Controller_finishedSync()
        {
            exit = true;
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            controller.StopRead();
            exit = true;
        }
    }
}
