﻿using OutlookSyncer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookSyncer.Controllers
{
    public class CommandLineController
    {
        public bool HasError { get; set; }
        public string ErrorMessage { get; set; }

        public string Server { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Group { get; set; }
        public string Password { get; set; }

        public List<CommandLineArg> CommandLineArgs {get; private set;}

        public CommandLineController(string[] arguments)
        {
            CommandLineArgs = new List<CommandLineArg>();
            foreach(var arg in arguments)
            {
                var commandLineArg = new CommandLineArg(arg);

                if (commandLineArg.HasError)
                {
                    HasError = true;
                    ErrorMessage = commandLineArg.ErrorMessage;
                    break;
                }

                CommandLineArgs.Add(commandLineArg);
            }

            if (HasError) return;

            if (CommandLineArgs.Count != 5)
            {
                HasError = true;
                ErrorMessage = string.Format(Notifications.NotifyChanges.CommandLIineController_Syntax, "Not enough arguments!");
                return;
            }

            var argumentsWithName = CommandLineArgs.Where(cmdarg => !string.IsNullOrEmpty(cmdarg.ParamName));

            if (argumentsWithName.Count() >0 && argumentsWithName.Count() <5)
            {
                ErrorMessage = string.Format(Notifications.NotifyChanges.CommandLIineController_Syntax, "Provide only named or only not named arguments!");
                return;
            }

            
            if (argumentsWithName.Any())
            {
                var serverArg = argumentsWithName.FirstOrDefault(arg => arg.ParamName.ToLower() == "server");

                if (serverArg == null)
                {
                    ErrorMessage = string.Format(Notifications.NotifyChanges.CommandLIineController_Syntax, "No Server provided!");
                    return;
                }

                Server = serverArg.ParamValue;

                var portArg = argumentsWithName.FirstOrDefault(arg => arg.ParamName.ToLower() == "port");

                if (portArg == null)
                {
                    ErrorMessage = string.Format(Notifications.NotifyChanges.CommandLIineController_Syntax, "No Server provided!");
                    return;
                }

                int port;

                if (!int.TryParse(portArg.ParamValue, out port))
                {

                    ErrorMessage = string.Format(Notifications.NotifyChanges.CommandLIineController_Syntax, "No valid Port provided!");
                    return;
                }
                Port = port;


                var usernameArg = argumentsWithName.FirstOrDefault(arg => arg.ParamName.ToLower() == "username");

                if (usernameArg == null)
                {
                    ErrorMessage = string.Format(Notifications.NotifyChanges.CommandLIineController_Syntax, "No Username provided!");
                    return;
                }

                UserName = usernameArg.ParamValue;

                var groupArg = argumentsWithName.FirstOrDefault(arg => arg.ParamName.ToLower() == "group");

                if (groupArg == null)
                {
                    ErrorMessage = string.Format(Notifications.NotifyChanges.CommandLIineController_Syntax, "No Group provided!");
                    return;
                }

                Group = groupArg.ParamValue;

                var passwordArg = argumentsWithName.FirstOrDefault(arg => arg.ParamName.ToLower() == "password");

                if (passwordArg == null)
                {
                    ErrorMessage = string.Format(Notifications.NotifyChanges.CommandLIineController_Syntax, "No Password provided!");
                    return;
                }

                Password = passwordArg.ParamValue;
            }
            else
            {
                Server = CommandLineArgs[0].ParamValue;
                var portStr = CommandLineArgs[1].ParamValue;

                int port;

                if (!int.TryParse(portStr, out port))
                {
                    
                    ErrorMessage = string.Format(Notifications.NotifyChanges.CommandLIineController_Syntax, "No valid Port provided!");
                    return;
                }
                Port = port;

                UserName = CommandLineArgs[2].ParamValue;

                Group = CommandLineArgs[3].ParamValue;

                Password = CommandLineArgs[4].ParamValue;
            }
        }
    }
}
