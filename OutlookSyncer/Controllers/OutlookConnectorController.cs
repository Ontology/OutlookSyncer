﻿
using OutlookSyncer.Models;
using OutlookSyncer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace OutlookSyncer.Controllers
{
    public delegate void FinishedSync();
    public class OutlookConnectorController
    {
        private WebSocket webSocket;
        private bool loginSuccess;

        Microsoft.Office.Interop.Outlook.Application objOutlook;
        Microsoft.Office.Interop.Outlook.NameSpace objMapi;


        public string ActiveFolder { get; private set; }

        public event FinishedSync finishedSync;

        private MailServiceAgent mailServiceAgent;

        private string server;
        private int port;
        private string userName;
        private string group;
        private string password;


        public OutlookConnectorController(string server, int port, string userName, string group, string password)
        {
            this.server = server;
            this.port = port;
            this.userName = userName;
            this.group = group;
            this.password = password;

            webSocket = new WebSocket("wss://" + server + ":" + port.ToString() + "/Outlook-Connector-Module.OutlookConnectorController?Module=851be88ef0804a1baac8c852c17d052b&Controller=eadaa051a14c4856afdf7b7ef04c99ba&Endpoint=" + Guid.NewGuid().ToString() + "&Session=" + Guid.NewGuid().ToString());
            webSocket.SslConfiguration.ClientCertificates = new System.Security.Cryptography.X509Certificates.X509CertificateCollection();
            webSocket.SslConfiguration.ClientCertificates.Add(new System.Security.Cryptography.X509Certificates.X509Certificate2("ontosockcert.pfx", ""));
            webSocket.OnClose += WebSocket_OnClose;
            webSocket.OnError += WebSocket_OnError;
            webSocket.OnMessage += WebSocket_OnMessage;
            webSocket.OnOpen += WebSocket_OnOpen;
            webSocket.Connect();
            mailServiceAgent = new MailServiceAgent();
            mailServiceAgent.PropertyChanged += MailServiceAgent_PropertyChanged;
        }

        private void MailServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.MailServiceAgent_MailItems)
            {
                
                if (mailServiceAgent.MailItems != null)
                {
                    Console.WriteLine("Readed Outlook-Mails: " + mailServiceAgent.MailItems.Count.ToString()  + " Mails");
                    var dictSend = new Dictionary<string, object>();

                    dictSend.Add("MessageType", "ViewItem");

                    var sendItems = new List<ViewItem>();

                    sendItems.Add(new ViewItem
                    {
                        ViewItemId = "MailDocs",
                        ViewItemClass = "Other",
                        ViewItemType = "Other",
                        ViewItemValue = mailServiceAgent.MailItems
                    });

                    dictSend.Add("Items", sendItems);

                    var jsonSend = Newtonsoft.Json.JsonConvert.SerializeObject(dictSend);

                    Console.WriteLine("Start sending Mails to Server...");
                    webSocket.Send(jsonSend);
                    Console.WriteLine("Mails sended to Server");
                    if (finishedSync != null)
                    {
                        finishedSync();
                    }
                }
                else
                {
                    Console.WriteLine("Mails cannot be read!");
                }
            }
        }

        private void Initialize()
        {
           
        }

        private void WebSocket_OnOpen(object sender, EventArgs e)
        {
            var dictLogin = new Dictionary<string, string>();

            dictLogin.Add("MessageType", "Change");
            dictLogin.Add("PropertyName", "Username");
            dictLogin.Add("Username", userName);

            var sendItem = Newtonsoft.Json.JsonConvert.SerializeObject(dictLogin);
            webSocket.Send(sendItem);


            dictLogin = new Dictionary<string, string>();

            dictLogin.Add("MessageType", "Change");
            dictLogin.Add("PropertyName", "Group");
            dictLogin.Add("Group", group);

            sendItem = Newtonsoft.Json.JsonConvert.SerializeObject(dictLogin);
            webSocket.Send(sendItem);

            dictLogin = new Dictionary<string, string>();

            dictLogin.Add("MessageType", "Change");
            dictLogin.Add("PropertyName", "Password");
            dictLogin.Add("Password", password);

            sendItem = Newtonsoft.Json.JsonConvert.SerializeObject(dictLogin);
            webSocket.Send(sendItem);



        }

        private void WebSocket_OnMessage(object sender, MessageEventArgs e)
        {
            try
            {
                var dictList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(e.Data);

                if (dictList.Any())
                {
                    dictList.ForEach(dict =>
                    {
                        if (dict["ViewItemId"].ToString() == "IsSuccessful_Login")
                        {
                            if ((bool)dict["LastValue"])
                            {
                                loginSuccess = (bool)dict["LastValue"];
                                SendMailItemsBegin();
                            }
                        }
                        
                    });

                    if (!loginSuccess)
                    {
                        Console.WriteLine("No valid credentials!");
                        Environment.Exit(-1);
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("No valid response from OntologyWebModules-Server");
                Environment.Exit(-1);
            }
            


        }

        private void SendMailItemsBegin()
        {
            if (State_Outlook == "Running")
            {
                var result = mailServiceAgent.GetMailItems(objOutlook);

                if (result)
                {
                    Console.WriteLine("Started reading mails...");

                }
                else
                {
                    Console.WriteLine("Mails cannot be read!");
                }
            }
            else
            {
                Console.WriteLine("Outlook is not running!");
            }
            
        }

        public void StopRead()
        {
            mailServiceAgent.StopRead();
        }

        public string State_Outlook
        {
            get
            {
                try
                {
                    objOutlook = (Microsoft.Office.Interop.Outlook.Application)Marshal.GetActiveObject("Outlook.Application");
                    var objActiveExplorer = objOutlook.ActiveExplorer();
                    if (objActiveExplorer != null)
                    {
                        ActiveFolder = objActiveExplorer.CurrentFolder.Name;
                        return "Running";
                    }
                    else
                    {
                        ActiveFolder = "";
                        return "Not Running";
                    }
                }
                catch (System.Exception ex)
                {
                    ActiveFolder = "";
                    return "Not Running";
                }



            }
        }

       

        private void WebSocket_OnError(object sender, ErrorEventArgs e)
        {
            Console.WriteLine(e.Message);
            Environment.Exit(-1);
        }

        private void WebSocket_OnClose(object sender, CloseEventArgs e)
        {
            
        }

        
        public void CloseSocket(string logMessage)
        {

        }

        
    }



}
