﻿using Microsoft.Office.Interop.Outlook;
using OutlookSyncer.Base;
using OutlookSyncer.Models;
using OutlookSyncer.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OutlookSyncer.Services
{
    public class MailServiceAgent : NotifyPropertyChange
    {
        private MAPIFolder rootFolder;
        private Microsoft.Office.Interop.Outlook.Application outlookApplication;

        private Thread getMailItemsAsyncController;


        public List<AppDocument> mailItems;
        public List<AppDocument> MailItems
        {
            get
            {
                return mailItems;
            }
            set
            {
                mailItems = value;
                RaisePropertyChanged(NotifyChanges.MailServiceAgent_MailItems);
            }
        }


        public bool GetMailItems(Microsoft.Office.Interop.Outlook.Application outlookApplication, MAPIFolder rootFolder = null)
        {
            this.rootFolder = rootFolder;
            this.outlookApplication = outlookApplication;

            mailItems = new List<AppDocument>();

            StopRead();

            getMailItemsAsyncController = new Thread(GetMailItemsAsync);
            getMailItemsAsyncController.Start(rootFolder);

            return true;
        }

        public void StopRead()
        {
            if (getMailItemsAsyncController != null)
            {
                try
                {
                    getMailItemsAsyncController.Abort();
                }
                catch(System.Exception ex)
                {

                }
            }
        }

        private void GetMailItemsAsync(object rootFolder)
        {
            var mapiFolder = (MAPIFolder)rootFolder;
            
            if (mapiFolder == null)
            {
                mapiFolder = outlookApplication.ActiveExplorer().CurrentFolder;
            }

            Console.WriteLine("Reading folder: " + mapiFolder.Name);

            var mailItems = GetMailItemsOfFolder(mapiFolder);

            MailItems = mailItems;

        }

        private List<AppDocument> GetMailItemsOfFolder(MAPIFolder mapiFolder)
        {
            var mailItems = new List<AppDocument>();

            for (int i = 1; i <= mapiFolder.Items.Count; i++)
            {
                var item = mapiFolder.Items[i] as Microsoft.Office.Interop.Outlook.MailItem;
                if (item != null)
                {
                    var itemType = item.GetType();
                    var strType = itemType.InvokeMember("MessageClass", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.GetProperty | System.Reflection.BindingFlags.Instance, null, mapiFolder.Items[i], new object[] { }).ToString();
                    if (strType == "IPM.Note")
                    {
                        var objMailItem = (Microsoft.Office.Interop.Outlook.MailItem)mapiFolder.Items[i];
                        var strTo = "";
                        foreach (Microsoft.Office.Interop.Outlook.Recipient objRecipient in objMailItem.Recipients)
                        {
                            strTo += strTo != "" ? ";" : "" + (objRecipient.Address != null ? objRecipient.Address.ToString() : "");
                        }

                        var objDict = new Dictionary<string, object>();

                        var mailItem = new AppDocument
                        {
                            Id = Guid.NewGuid().ToString().Replace("-", ""),
                            Dict = new Dictionary<string, object> {
                                {"EntryID", objMailItem.EntryID},
                                {"CreationDate",objMailItem.CreationTime},
                                {"Folder",mapiFolder.Name},
                                {"SemItemPresent",false},
                                {"SenderEmailAddress",objMailItem.SenderEmailAddress},
                                {"SenderName",objMailItem.SenderName},
                                {"Subject",objMailItem.Subject},
                                {"To",strTo}}
                        };
                        mailItems.Add(mailItem);
                    }
                }

            }

            foreach (Microsoft.Office.Interop.Outlook.MAPIFolder objSubFolder in mapiFolder.Folders)
            {
                mailItems.AddRange(GetMailItemsOfFolder(objSubFolder));

            }

            return mailItems;
        }
    }
}
